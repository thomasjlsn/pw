#!/usr/bin/env sh
# Copyright (C) 2013-2016 Sören Tempel
# Copyright (C) 2022 Thomas Ellison
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

umask 077

STORE_DIR="${PASSWORD_STORE_DIR:-${XDG_DATA_HOME:-$HOME/.local/share}/password-store}"


abort() {
  printf '%s\n' "$1" 1>&2
  exit 1
}


gpg() {
  if [ -r "$STORE_DIR/.gpg-id" ] && [ -z "$PASSWORD_STORE_KEY" ]; then
    read -r PASSWORD_STORE_KEY < "$STORE_DIR/.gpg-id"
  fi

  if [ -n "$PASSWORD_STORE_KEY" ]; then
    gpg2 --quiet --yes --batch --recipient "$PASSWORD_STORE_KEY" "$@"
  else
    gpg2 --quiet --yes --batch --default-recipient-self "$@"
  fi
}


make_tmpfile() {
  shm="/dev/shm"

  if [ -d "$shm" ] && [ -w "$shm" ] && [ -x "$shm" ]; then
    tmpdir="$shm"
  else
    tmpdir="/tmp"
  fi

  printf '%s' "$(mktemp "$tmpdir/tmp.XXXXXXXXXX")"
}


read_password() {
  if [ -t 0 ]; then
    printf 'Password: '
    stty -echo
    IFS= read -r password
    stty echo
    printf '\n'
  else
    password="$(cat)"
  fi
}


pw_add() {
  [ -f "$2" ] && abort "The entry \"$1\" already exists."

  read_password

  [ -z "$password" ] && abort "No password was specified."

  mkdir -p "${2%/*}"
  printf '%s\n' "$password" | gpg --encrypt --output "$2"
}


pw_edit() {
  tmpfile="$(make_tmpfile)"
  trap 'shred -fuz "$tmpfile" || rm -f "$tmpfile"; exit 0' EXIT 0 1 2 3 15

  [ -f "$2" ] && gpg --output "$tmpfile" --decrypt "$2"

  EDITOR="${EDITOR:-vim}"

  case "$EDITOR" in
    vim|nvim) "$EDITOR" -c 'set viminfo= nobackup noswapfile noundofile nowritebackup' "$tmpfile" ;;
    *) "$EDITOR" "$tmpfile" ;;
  esac || abort "Could not open file with $EDITOR."

  mkdir -p "${2%/*}"
  gpg --output "$2" --encrypt "$tmpfile"
}


pw_show() {
  [ -f "$2" ] || abort "The entry \"$1\" does not exist."

  gpg --decrypt "$2"
}


pw_qr() {
  command -v qrencode >/dev/null || abort "\"qrencode\" is not installed."

  pw_show "$@" 2>/dev/null | head -n 1 | qrencode -t "${PASSWORD_STORE_QR_TYPE:-ANSI}"
}


pw_find() {
  find "$STORE_DIR" -name '*.gpg' | sed "s%^$STORE_DIR/\?%%; s/\.gpg$//" | sort | grep -Ei "$1"
}


pw_gen() {
  [ -e "/dev/urandom" ] || abort "Unable to use /dev/urandom to generate a password."

  case "$1" in
    [0-9]*) n="$1" ;;
    *) n=32 ;;
  esac

  printf '%s\n' "$(tr -dc "${PASSWORD_STORE_CHARACTER_SET:-[:graph:]}" < /dev/urandom | head -c $n)"
}


pw_grep() {
  find "$STORE_DIR" -name '*.gpg' | while read -r entry_path; do
    result="$(gpg --decrypt "$entry_path" | grep -Ei "$1")"

    if [ "$result" != "" ]; then
      entry="$(printf '%s' "$entry_path" | sed "s%^$STORE_DIR/\?%%; s/\.gpg$//" )"

      if [ -t 1 ]; then
        printf '\e[34;1m%s\e[0m:\n%s\n\n' "$entry" "$result"
      else
        printf '%s:\n%s\n\n' "$entry" "$result"
      fi
    fi
  done
}


pw_list() {
  dir="$STORE_DIR/$1"

  [ -d "$dir" ] || abort "The directory \"$1\" does not exist."

  tree "$dir" -CFP '*.gpg' --dirsfirst --noreport | sed "
    s%.*$STORE_DIR.*/\?%[1mPassword Store${1+\n[34m${1%%/}/[0m}%;
    s%/$%[34;1m/[0m%;
    s/\.gpg//;
  "
}


main() {
  [ $# -gt 2 ] && abort "pw doesn't accept more than two arguments."

  if [ -z "$1" ]; then
    pw_list ""
    exit 0
  fi

  case "$1" in
    "-h"|"--help"|"help") man pw ;;
    "add"|"edit"|"show"|"qr")
      [ -z "$2" ] && abort "Command \"$1\" requires an additional argument, ENTRY."
      "pw_$1" "$2" "$STORE_DIR/$2.gpg"
      ;;
    "find"|"grep")
      [ -z "$2" ] && abort "Command \"$1\" requires an additional argument, PATTERN."
      "pw_$1" "$2"
      ;;
    "gen"|"list") "pw_$1" "$2" ;;
    *) abort "Unknown command \"$1\", use one of: add edit find gen grep help show qr" ;;
  esac
}


main "$@"
