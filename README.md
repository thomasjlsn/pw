# PW

A simple password manager (forked from [tpm](https://github.com/nmeum/tpm))

[TOC]


## DESCRIPTION

**pw** is a shell script based off of **tpm** that is
largely compatible with [pass](https://www.passwordstore.org/).

**pw** is meant to bridge the gap between the ultra-minimal program **tpm** and
the everything-under-the-sun program **pass**. **pw**'s main goal is to adopt
useful features of **pass** while retaining a minimal, easy to understand
codebase.


## INSTALLATION

Installing **pw** with make:

```
git clone https://gitlab.com/thomasjlsn/pw
cd pw
sudo make install
```

Installing the latest stable release of **pw** from the AUR:

```
yay -S pw
```

Installing from the latest commit of **pw** from the AUR:

```
yay -S pw-git
```

Installing **pw** manually (adjust file paths as needed):

```
git clone https://gitlab.com/thomasjlsn/pw
cd pw
cp pw.sh ~/.local/bin/pw
chmod +x ~/.local/bin/pw

# To enable bash completion
cp completion/bash_completion ~/.local/share/bash-completion/completions/_pw

# To enable zsh completion
cp completion/zsh_completion ~/.local/share/zsh/site-functions/_pw
```

## BASIC USAGE

#### CREATING AND EDITING ENTRIES

Create a new entry called 'gitlab.com':

```
pw add gitlab.com
```

Entries can be organized in different directories.

Create a new entry called 'code/gitlab.com':

```
pw add code/gitlab.com
```

Edit your 'code/gitlab.com' password:

(note: when using vim or nvim as `$EDITOR`, options that may leak sensitive
data are automatically disabled)

```
pw edit code/gitlab.com
```

#### DISPLAYING ENTRIES

Write the 'code/gitlab.com' entry to standard output:

```
pw show code/gitlab.com
```

Generate a QR code containing the password for 'code/gitlab.com':

(note: this only encodes the first line of the entry)

```
pw qr code/gitlab.com
```

#### FINDING ENTRIES

Search for entries whose name contain the pattern 'git':

```
pw find git
```

Search the contents of all password entries for the pattern 'qwerty':

```
pw grep qwerty
```

List the contents of the password store:

```
pw list
```

List the contents of the 'code' subdirectory of the password store:

```
pw list code
```


## ADVANCED USAGE

### MORE THAN JUST PASSWORDS

Since **pw** stores simple encrypted text files, you can add any additional
information to the entries you want. For example, I add all my login
credentials to an entry, it typically looks something like this:

```
randompassword
name:   my name
user:   myusername
email:  me@email.com

additional notes if applicable
```

This also means that entries can be anything you want: addresses, phone
numbers, etc. Any sensitive information you want to store and have easy
access to.

### INTEGRATION WITH OTHER PROGRAMS

Instead of trying to integrate tons of functionality into **pw** directly,
**pw** uses stdin/stdout to communicate with other programs. This allows for
far more flexible usage and configuration.

#### PASSWORD GENERATION

Generate a random password that is 20 characters long (defaults to 32
characters if not specified):

```
pw gen 20
```

Generating a password for a new entry 'code/gitlab.com' with **pw**'s `gen` command:

```
pw gen | pw add code/gitlab.com
```

For more precise control over the passwords you generate, you can use any
password generator you like. For example, pwgen(1):

```
pwgen -sy1 32 | pw add code/gitlab.com
```

#### CLIPBOARD INTEGRATION

Copy your 'code/gitlab.com' password to the clipboard using xclip(1):

```
pw show code/gitlab.com | xclip -r -sel clip
```

This could be simplified with a shell function (If you store additional info in
your entry as shown above, throw in a `head -n 1`):

```
pw-clip() {
  pw show "${1?no password specified}" | head -n 1 | xclip -r -sel clip
}
```

And used with:

```
pw-clip system/root
```

#### MANAGING PASSWORDS IN OTHER PROGRAMS

You can use **pw** with other programs that allow interpolation of shell
commands in their config. For example, here's how I set my password in my
`muttrc`:

```
set imap_pass = `pw show email/me@email.com | head -n 1`
set smtp_pass = `pw show email/me@email.com | head -n 1`
```

### MANAGING THE PASSWORD STORE

Managing the password store is no different than managing files and directories
as you normally would. You can use your standard \*nix tools like `cp`, `mkdir`,
`mv`, and `rm`. You could also your file manager of choice to make the job even
easier.


## CONFIGURATION

#### ENVIRONMENT

- `PASSWORD_STORE_DIR` The directory where passwords are stored (default:
  `~/.local/share/password-store`).

- `XDG_DATA_HOME` **pw** respects XDG standards. If `PASSWORD_STORE_DIR` is not
  set, **pw** tries using `XDG_DATA_HOME` to configure the storage directory.
  If `XDG_DATA_HOME` is not set, **pw** defaults to using `~/.local/share`.

- `PASSWORD_STORE_KEY` The GPG key used to encrypt files (default: self).

- `PASSWORD_STORE_CHARACTER_SET` The character set used for password
  generation. See `man tr` (under `SETs`) for more info. (default `[:graph:]`)

- `PASSWORD_STORE_QR_TYPE` The type of qr code that is generated. See `man
  qrencode` (the `--type` argument) for all options. (default: `ANSI`)


#### FILES

- `~/.local/share/password-store` The default storage directory.

- `~/.local/share/password-store/.gpg-id` Provided for compatibility with pass(1).


## SEE ALSO

gpg2(1), pass(1), pwgen(1), qrencode(1), tr(1), xclip(1)
