PROG ?= pw
PREFIX ?= /usr/local
SHARE ?= $(PREFIX)/share
BIN ?= $(PREFIX)/bin/$(PROG)
MANPAGE ?= $(SHARE)/man/man1/$(PROG).1
LICENSE ?= $(SHARE)/licenses/$(PROG)/LICENSE
BCOMP ?= $(SHARE)/bash-completion/completions/_$(PROG)
ZCOMP ?= $(SHARE)/zsh/site-functions/_$(PROG)

install:
	install -Dm755 pw.sh "$(BIN)"
	install -Dm644 pw.1 "$(MANPAGE)"
	install -Dm644 LICENSE "$(LICENSE)"
	install -Dm644 completion/bash_completion "$(BCOMP)"
	install -Dm644 completion/zsh_completion "$(ZCOMP)"

uninstall:
	$(RM) "$(BIN)" \
	      "$(MANPAGE)" \
	      "$(LICENSE)" \
	      "$(BCOMP)" \
	      "$(ZCOMP)"

clean:
	$(RM) .SRCINFO
	$(RM) PKGBUILD

aur:
	mkdir aur
	git clone ssh://aur@aur.archlinux.org/pw.git aur/

aur-git:
	mkdir aur-git
	git clone ssh://aur@aur.archlinux.org/pw-git.git aur-git/

aur-update: aur
	cp PKGBUILD-stable PKGBUILD
	makepkg --printsrcinfo > .SRCINFO
	cp PKGBUILD .SRCINFO aur/
	make clean

aur-update-git: aur-git
	cp PKGBUILD-git PKGBUILD
	makepkg --printsrcinfo > .SRCINFO
	cp PKGBUILD .SRCINFO aur-git/
	make clean
